<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('nosotros', 'UsController@index')->name('nosotros');


Route::get('recetas','RecipesController@index')->name('recetas');

Route::get('registro', function () {
    return view('auth.register');
})->name('registro');
Route::get('administracion', function () {
    
})->name('administracion');
Route::get('publicacion', 'PublishController@index' )->name('publicacion');

Route::get('recetas/receta','RecipeController@index')->name('receta');


Route::get('tips', 'TipsController@index')->name('tips');


Route::group(['prefix' => 'index'], function () {
    Route::get('{person?}', function ($name) {
    
    })->name('usuario');
    Route::get('{person?}', function ($name) {
    
    })->name('admin');
});


Route::get('/', 'IndexController@index')->name('index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
