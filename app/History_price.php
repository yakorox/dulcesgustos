<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History_price extends Model
{
    protected $table='history_prices';
    protected $fillable=[
        'date','price','product_id',
    ];
}
