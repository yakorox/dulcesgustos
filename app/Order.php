<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table='orders';
    protected $fillable=[
        'date','tax','total_order','state','user_id','employee_id',
    ];
}
