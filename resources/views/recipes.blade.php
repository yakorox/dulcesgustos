@extends('layouts.init')

    @section('title')
        Dulces Gustos - Recetas
    @endsection

    @section('head')
        <link rel="stylesheet" href="{{ asset('css/nav.css')}}">
        <link rel="stylesheet" href="{{ asset('css/recetas.css')}}">
    @endsection


    @section('content')
        @include('partials.header')
        <main class="container-fluid" id="prueba1">
            <section class="row bread-nav w-100">
                <nav aria-label="breadcrumb " class="col-12 w-100">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('index')}}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Recetas</li>
                    </ol>
                </nav>
            </section>
            <section  class="row fil-total">
                <section class="col-12 col-md-2 navig">
                    <nav class="navigation">
                        <h3 class="hidden-sm-down">Categoras:</h3>
                        <ul class="mainmenu d-flex d-md-block">
                            <li><a href="#">Personas</a>
                                <ul class="submenu">
                                    <li><a href="#">Celiaco</a></li>
                                    <li><a href="#">Diabetica</a></li>
                                    <li><a href="#">Fitness</a></li>
                                    <li><a href="#">Alergica</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Producto</a>
                                <ul class="submenu">
                                    <li><a href="#">Pan</a></li>
                                    <li><a href="#">Tortas</a></li>
                                    <li><a href="#">Galletas</a></li>
                                    <li><a href="#">Chocolate</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Celebración</a>
                                <ul class="submenu">
                                    <li><a href="#">Cumpleaños</a></li>
                                    <li><a href="#">Boda</a></li>
                                    <li><a href="#">ETC</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Dificulta</a>
                                <ul class="submenu">
                                    <li><a href="#">Facil</a></li>
                                    <li><a href="#">Media</a></li>
                                    <li><a href="#">Dificil</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </section>
                <section class="col-12 col-md-10 all-recetas">  
                    <section  class="row items-container d-flex justify-content-around caption-style">
                        <section class="col-4 item item-visible  col-lg-3 img-thumbnail items-1">
                            <a href="{{ route('receta')}}">
                                <p class="title-receta">Fresa Diamante</p>
                                <section class="caption">
                                        <section class="blur"></section>
                                        <section class="d-flex flex-column align-items-center justify-content-around caption-text">
                                            <h1>Fresa Diamante</h1>
                                            <p class="px-2">Ricas galletas, con masa de hojaldre y una dulce fresa en medio.</p>
                                        </section>
                                </section>
                            </a>
                        </section>
                        <section class="col-4 item item-visible col-lg-3 img-thumbnail items-2">
                            <a href="#">
                                <p class="title-receta">Soufleglace Chocolate</p>
                                <section class="caption">
                                        <section class="blur"></section>
                                        <section class="d-flex flex-column align-items-center justify-content-around caption-text">
                                            <h1>Soufleglace Chocolate</h1>
                                            <p class="px-2">Esquisito manjar de chocolate, que deleitara tu paladar</p>
                                        </section>
                                </section>
                            </a>
                        </section>
                        <section class="col-4 item item-visible col-lg-3 img-thumbnail items-3">
                            <a href="#">
                                <p class="title-receta">Torta Damero</p>
                                <section class="caption">
                                        <section class="blur"></section>
                                        <section class="d-flex flex-column align-items-center justify-content-around caption-text">
                                            <h1>Torta Damero</h1>
                                            <p class="px-2">Rica torta de ponque de chocolate y vainilla, con forma de tablero de ajedrez.</p>
                                        </section>
                                </section>
                            </a>
                        </section>
                        <section class="col-4 item item-visible col-lg-3 img-thumbnail items-4">
                            <a href="#">
                                <p class="title-receta">Ponques de Fresa</p>
                                <section class="caption">
                                        <section class="blur"></section>
                                        <section class="d-flex flex-column align-items-center justify-content-around caption-text">
                                            <h1>Ponques de Fresa</h1>
                                            <p class="px-2">Ponques a base de fresas, con crema chantilly.</p>
                                        </section>
                                </section>
                            </a>
                        </section>
                        <section class="col-4 item item-visible col-lg-3 img-thumbnail items-5">
                            <a href="">
                                <p class="title-receta">Tarta de Naranja</p>
                                <section class="caption">
                                        <section class="blur"></section>
                                        <section class="d-flex flex-column align-items-center justify-content-around caption-text">
                                            <h1>Tarta de Naranja</h1>
                                            <p class="px-2">Tarta de naranja, base de masa sable y de relleno crema de naranja.</p>
                                        </section>
                                </section>
                            </a>
                        </section>
                        <section class="col-4 item item-visible col-lg-3 img-thumbnail items-6">
                            <a href="#">
                                <p class="title-receta">Muffin</p>
                                <section class="caption">
                                        <section class="blur"></section>
                                        <section class="d-flex flex-column align-items-center justify-content-around caption-text">
                                            <h1>Muffin de Mora</h1>
                                            <p class="px-2">Ricos muffin rellenos de mermelada de mora.</p>
                                        </section>
                                </section>
                            </a>
                        </section>
                        <section class="col-4 item item-visible col-lg-3 img-thumbnail items-7">
                            <a href="#">
                                <p class="title-receta">Torta de Cumpleaños</p>
                                <section class="caption">
                                        <section class="blur"></section>
                                        <section class="d-flex flex-column align-items-center justify-content-around caption-text">
                                            <h1>Torta de Cumpleaños</h1>
                                            <p class="px-2">Torta decorada con fondant, ponques de vainilla. </p>
                                        </section>
                                </section>
                            </a>
                        </section>
                        <section class="col-4 item item-visible col-lg-3 img-thumbnail items-8">
                            <a href="#">
                                <p class="title-receta">Chispas</p>
                                <section class="caption">
                                        <section class="blur"></section>
                                        <section class="d-flex flex-column align-items-center justify-content-around caption-text">
                                            <h1>Chispas de Chocolate</h1>
                                            <p class="px-2">Delisiosas galletas con chispas de chocolate.</p>
                                        </section>
                                </section>
                            </a>
                        </section>
                        <section class="col-4 item item-visible col-lg-3 img-thumbnail items-9">
                            <a href="receta.html">
                                <p class="title-receta">Torta Fria de Fresas</p>
                                <section class="caption">
                                        <section class="blur"></section>
                                        <section class="d-flex flex-column align-items-center justify-content-around caption-text">
                                            <h1>Torta Fria de Fresas</h1>
                                            <p class="px-2">La base de esta delisiosa torta es una galleta de vainilla, la crema de leche condensada y fresas. .</p>
                                        </section>
                                </section>
                            </a>
                        </section>
                        <section class="col-4 item item-visible col-lg-3 img-thumbnail items-10">
                            <a href="#">
                                <p class="title-receta">Cachitos</p>
                                <section class="caption">
                                        <section class="blur"></section>
                                        <section class="d-flex flex-column align-items-center justify-content-around caption-text">
                                            <h1>Cachitos de hojaldre</h1>
                                            <p class="px-2">Cachitos rellenos con mermelada de fresa y preparados con masa de hojaldre</p>
                                        </section>
                                </section>
                            </a>
                        </section>
                        <section class="col-4 item item-visible col-lg-3 img-thumbnail items-11">
                            <a href="#">
                                <p class="title-receta">Dulces bocados</p>
                                <section class="caption">
                                        <section class="blur"></section>
                                        <section class="d-flex flex-column align-items-center justify-content-around caption-text">
                                            <h1>Dulces bocados</h1>
                                            <p class="px-2">Ponquesitos rellenos de crema de fresa, y en baño de chocolate con fresa.</p>
                                        </section>
                                </section>
                            </a>
                        </section>
                        <section class="col-4 item item-visible col-lg-3 img-thumbnail items-12">
                            <a href="#">
                                <p class="title-receta">Ponquesitos de vainilla</p>
                                <section class="caption">
                                        <section class="blur"></section>
                                        <section class="d-flex flex-column align-items-center justify-content-around caption-text">
                                            <h1>Ponquesitos de vainilla</h1>
                                            <p class="px-2">Ponques de vainilla, decorados con crema de mantequilla y figuras en fondant.</p>
                                        </section>
                                </section>
                            </a>
                        </section>
                        <section class="col-4 item item-visible col-lg-3 img-thumbnail items-13">
                            <a href="#">
                                <p class="title-receta">Tora de Chocolate</p>
                                <section class="caption">
                                        <section class="blur"></section>
                                        <section class="d-flex flex-column align-items-center justify-content-around caption-text">
                                            <h1>Tora de Chocolate</h1>
                                            <p class="px-2">Torta de chocolate con crema de cacao y vainilla.</p>
                                        </section>
                                </section>
                            </a>
                        </section>
                        <section class="col-4 item item-visible col-lg-3 img-thumbnail items-14">
                            <a href="#">
                                <p class="title-receta">Mil hojas</p>
                                <section class="caption">
                                        <section class="blur"></section>
                                        <section class="d-flex flex-column align-items-center justify-content-around caption-text">
                                            <h1>Mil hojas</h1>
                                            <p class="px-2">Laminas de hojaldre con crema de vainilla y cafe. </p>
                                        </section>
                                </section>
                            </a>
                        </section>
                        <section class="col-4 item item-visible col-lg-3 img-thumbnail items-15">
                            <a href="#">
                                <p class="title-receta">Red Velvet</p>
                                <section class="caption">
                                        <section class="blur"></section>
                                        <section class="d-flex flex-column align-items-center justify-content-around caption-text">
                                            <h1>Red Velvet</h1>
                                            <p class="px-2">Torta Red Velvet, rellena de crema de mantequilla. .</p>
                                        </section>
                                </section>
                            </a>
                        </section>
                    </section> 
                    
                    <section class="row d-flex justify-content-center">
                        <section class="pagination-indicador"></section>
                        <ul class="pagination-container"></ul>
                    </section>
                </section>
            </section>
        </main>
        @include('partials.footer')
    @endsection

    @section('bodyscript')
        <script src="{{ asset('js/jquery-3.3.1.slim.min.js')}}"></script>
        <script src="{{ asset('js/jquery.jold.paginator.min.js')}}"></script>
        <script src="{{ asset('js/nav.js')}}"></script>
        <script src="{{ asset('js/recetas.js')}}"></script>
    @endsection