
@extends('layouts.init')


@section('title')
Dulces Gustos - Registrarse
@endsection

@section('head')
    <link rel="stylesheet" href="{{ asset('css/navs.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap--s.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/registross.css') }}">
@endsection

@section('content')
    @include('partials.header')
    <main class="container-fluid mb-5">
        <section class="row ">
            <section class="col-sm-8 col-sm-offset-2">
                <section class="panel">
                    <section class="panel-heading">
                        <h3 class="panel-title">{{ __('Register') }}</h3>
                    </section>
                    <section class="panel-body">
                        <form id="signupForm1" method="post" action="{{ route('register') }}">

                            <section class="form-group ">
                                <label class="col-sm-5  control-label" for="firstname">Nombre</label>
                                <section class="col-12">
                                    <input type="text" class="form-control" id="firstname" name="firstname" placeholder="Pepito Andres" />
                                </section>
                            </section>
                            <section class="form-group row">
                                <label class="col-sm-5  control-label" for="lastname">Apellido</label>
                                <section class="col-12">
                                    <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Buitrago Jackson" />
                                </section>
                            </section>
                            <section class="form-group row">
                                <label class="col-sm-5  control-label" for="address">Dirección</label>
                                <section class="col-12">
                                    <input type="text" class="form-control" id="address" name="address" placeholder="mi casa calle 5" />
                                </section>
                            </section>
                            <section class="form-group row">
                                <label class="col-sm-5  control-label" for="email1">Correo</label>
                                <section class="col-12">
                                    <input type="text" class="form-control" id="email1" name="email1" placeholder="elpelusa@sabe.es" />
                                </section>
                            </section>
                            <section class="form-group row">
                                <label class="col-sm-5  control-label" for="email1_confirm">Confirmar Correo</label>
                                <section class="col-12">
                                    <input type="text" class="form-control" id="email1_confirm" name="email1_confirm" placeholder="elpelusa@sabe.es" />
                                </section>
                            </section>
                            <section class="form-group row">
                                <label class="col-sm-5  control-label" for="password">Contrase&ntilde;a</label>
                                <section class="col-12">
                                    <input type="password" class="form-control" id="password" name="password" placeholder="Contrase&ntilde;a" />
                                </section>
                            </section>
                            <section class="form-group row">
                                <label class="col-sm-5  control-label" for="confirm_password">Confirmar cotrase&ntilde;a</label>
                                <section class="col-12">
                                    <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Confirmar contrase&ntilde;a" />
                                </section>
                            </section>
                            
                            <section class="form-group row">
                                <label class="col-3 control-label" for="sex">Sexo</label>
                                <section class="col-3">
                                    <section class="custom-controls-stacked">
                                        <label class="custom-control custom-radio">
                                            <input id="male" name="sex" type="radio" class="custom-control-input" checked>
                                            <span class="custom-control-indicator "></span>
                                            <span class="custom-control-description">Masculino</span>
                                        </label>
                                        <label class="custom-control custom-radio">
                                            <input id="female" name="sex" type="radio" class="custom-control-input">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">Femenino</span>
                                        </label>
                                    </section>
                                </section>
                                <section class="col-6">
                                    <section class="checkbox">
                                        <label>
                                            <input type="checkbox" id="agree1" name="agree1" value="agree" />Por favor acepte nuestras politicas.
                                        </label>
                                    </section>
                                </section>
                            </section>
                            <section class="form-group row">
                                <section class="d-flex flex-column align-items-center col-12 justify-content-center">
                                    <button type="submit" class="btn btn-success" name="signup1" value="Sign up">Registrarse</button>
                                </section>
                            </section>
                        </form>
                    </section>
                </section>
            </section>
        </section>
    </main>

    @include('partials.footer')
@endsection

@section('bodyscript')
    <script src="{{ asset('js/nav.js') }}"></script>
    <script src="{{ asset('js/formulario.js') }}"></script>
    <script src="{{ asset('js/jquery.validate.min.js') }}"></script>
@endsection

