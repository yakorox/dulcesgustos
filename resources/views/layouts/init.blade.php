<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/bootstrap.min.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Fontdiner+Swanky|Oswald:200,300,500,600" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/fontello.css')}}">
    @yield('head')
    
</head>

<body>
    @yield('content')



    <script src="{{ asset('js/tether.min.js')}}"></script>
    <script src="{{ asset('js/jquery.min.js')}}"></script>
    <script src="{{ asset('js/bootstrap.min.js')}}"></script>
    @yield('bodyscript')
</body>
</html>