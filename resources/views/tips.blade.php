@extends('layouts.init')

@section('title')
    Dulces Gustos - Tips
@endsection

@section('head')
    <link rel="stylesheet" href="{{ asset('css/nav.css')}}">
    <link rel="stylesheet" href="{{ asset('css/tips.css')}}">
@endsection



@section('content')

    @include('partials.header')
    <main class="container-fluid">
        <section class="row bread-nav w-100">
            <nav aria-label="breadcrumb " class="col-12 w-100">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('index')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Tips</li>
                </ol>
            </nav>
        </section>
        <section class="row">
            <section class="col-12 col-lg-3 mb-2 tips-1 ">
                <h3 class="title-tips text-center">Tips Ingredientes</h3>
                <section class="row content-tips card-group">
                    <section class="col-12 card mb-2 p-2">
                        <img class="card-img-top" src="img/tips/ingredientes.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Huevos</h5>
                            <p class="card-text">Siempre usa huevos frescos, que estén a temperatura ambiente. Los refrigerados demoran en airearse.
                            </p>
                        </div>
                        <div class="card-footer">
                            <small class="text-muted">Publicado hace 3 minutos.</small>
                        </div>
                        </div>
                    </section>
                    <section class="col-12 card mb-2 p-2">
                        <img class="card-img-top" src="img/tips/harina.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Harina</h5>
                            <p class="card-text">Si quieres usar harina integral, sustituye en la receta la mitad de la harina blanca.
                        <div class="card-footer">
                            <small class="text-muted">Publicado hace 3 minutos.</small>
                        </div>
                        </div>
                    </section>
                    <section class="col-12 card mb-2 p-2">
                        <img class="card-img-top" src="img/tips/chocolate.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Chocolate</h5>
                            <p class="card-text">Nunca derritas chocolate directamente en la hornilla, se debe derretir a baño maría.</p>
                        </div>
                        <div class="card-footer">
                            <small class="text-muted">Publicado hace 3 minutos.</small>
                        </div>
                        </div>
                    </section>
                    <section class="col-12 card mb-2 p-2">
                        <img class="card-img-top" src="img/tips/crema.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Crema chantilly</h5>
                            <p class="card-text">Si preparas chantilly ten una crema de leche refrigerada mínimo 24 horas.</p>
                        </div>
                        <div class="card-footer">
                            <small class="text-muted">Publicado hace 3 minutos.</small>
                        </div>
                        </div>
                    </section>



                </section>
            </section>
            <section class="col-12 col-lg-3 mb-2 tips-2">
                <h3 class="title-tips text-center">Tips Panes</h3>
                <section class="row content-tips card-group">
                    <section class="col-12 card mb-2 p-2">
                        <img class="card-img-top" src="img/Tips/amasado.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Amasado</h5>
                            <p class="card-text">Cuando amases no pongas tanta harina en la mesa, siempre de a poco, sino la masa tiende a endurecerse.
                                .</p>
                        </div>
                        <div class="card-footer">
                            <small class="text-muted">Publicado hace 3 minutos.</small>
                        </div>
                        </div>
                    </section>
                    <section class="col-12 card mb-2 p-2">
                        <img class="card-img-top" src="img/tips/mantequilla.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Sabor</h5>
                            <p class="card-text">Se puede utilizar mantequilla o margarina, la diferencia está en el buen sabor que deja la mantequilla.</p>
                        </div>
                        <div class="card-footer">
                            <small class="text-muted">Publicado hace 3 minutos.</small>
                        </div>
                        </div>
                    </section>
                    <section class="col-12 card mb-2 p-2">
                        <img class="card-img-top" src="img/tips-nuevo.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">¿Cómo preservar la levadura?</h5>
                            <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Officia,
                                ullam.</p>
                        </div>
                        <div class="card-footer">
                            <small class="text-muted">Publicado hace 3 minutos.</small>
                        </div>
                        </div>
                    </section>
                    <section class="col-12 card mb-2 p-2">
                        <img class="card-img-top" src="img/tips/leudar.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Levadura</h5>
                            <p class="card-text">Probar la levadura antes de incorporar al pan ayuda en el levado del mismo.</p>
                        </div>
                        <div class="card-footer">
                            <small class="text-muted">Publicado hace 3 minutos.</small>
                        </div>
                        </div>
                    </section>



                </section>
            </section>
            <section class="col-12 col-lg-3 mb-2 tips-3">
                <h3 class="title-tips text-center">Tips Galletas</h3>
                <section class="row content-tips card-group">
                    <section class="col-12 card mb-2 p-2">
                        <img class="card-img-top" src="img/tips/papel.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Horneado</h5>
                            <p class="card-text">Al hornear galletas usa papel encerado para que no se peguen.</p>
                        </div>
                        <div class="card-footer">
                            <small class="text-muted">Publicado hace 3 minutos.</small>
                        </div>
                        </div>
                    </section>
                    <section class="col-12 card mb-2 p-2">
                        <img class="card-img-top" src="img/tips/enfriar.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Enfriar</h5>
                            <p class="card-text">Enfría galletas o cupcakes después de salidos del horno utilizando una rejilla metálica, ya que si dejas en las latas o moldes, seguirán cocinándose.</p>
                        </div>
                        <div class="card-footer">
                            <small class="text-muted">Publicado hace 3 minutos.</small>
                        </div>
                        </div>
                    </section>
                    <section class="col-12 card mb-2 p-2">
                        <img class="card-img-top" src="img/tips/textura.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Textura</h5>
                            <p class="card-text"> Obtén unas galletas con una textura más blanda cambiando el azúcar blanca de la receta por la morena.</p>
                        </div>
                        <div class="card-footer">
                            <small class="text-muted">Publicado hace 3 minutos.</small>
                        </div>
                        </div>
                    </section>
                    <section class="col-12 card mb-2 p-2">
                        <img class="card-img-top" src="img/tips/masa.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Refrigerar la masa</h5>
                            <p class="card-text">Unas galletas con mejor textura se obtienen si refrigeramos la masa por lo menos unas seis horas.</p>
                        </div>
                        <div class="card-footer">
                            <small class="text-muted">Publicado hace 3 minutos.</small>
                        </div>
                        </div>
                    </section>



                </section>
            </section>
            <section class="col-12 col-lg-3 mb-2 tips-4">
                <h3 class="title-tips text-center">Tips Tortas</h3>
                <section class="row content-tips card-group">
                    <section class="col-12 card mb-2 p-2">
                        <img class="card-img-top" src="img/tips/cremas.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Cremas en las tortas</h5>
                            <p class="card-text">Nunca pongas mousse a base de crema de leche y gelatina encima de una torta caliente, ya que se derretirá.
                                </p>
                        </div>
                        <div class="card-footer">
                            <small class="text-muted">Publicado hace 3 minutos.</small>
                        </div>
                        </div>
                    </section>
                    <section class="col-12 card mb-2 p-2">
                        <img class="card-img-top" src="img/tips/color.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Color</h5>
                            <p class="card-text">Para acentuar el color de tus tortas, usa colorantes para repostería.</p>
                        </div>
                        <div class="card-footer">
                            <small class="text-muted">Publicado hace 3 minutos.</small>
                        </div>
                        </div>
                    </section>
                    <section class="col-12 card mb-2 p-2">
                        <img class="card-img-top" src="img/tips/horneado.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Horneado</h5>
                            <p class="card-text">Si horneas una torta muy grande la temperatura debe ser de 150 a 160°C.
                                ullam.</p>
                        </div>
                        <div class="card-footer">
                            <small class="text-muted">Publicado hace 3 minutos.</small>
                        </div>
                        </div>
                    </section>
                    <section class="col-12 card mb-2 p-2">
                        <img class="card-img-top" src="img/tips/colorante.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Tono oscuro</h5>
                            <p class="card-text"> Para que una torta de chocolate quede con un tono más oscuro y llamativo no debes batir mucho la mezclaLorem ipsum dolor sit amet consectetur adipisicing elit. Officia,
                                ullam.</p>
                        </div>
                        <div class="card-footer">
                            <small class="text-muted">Publicado hace 3 minutos.</small>
                        </div>
                        </div>
                    </section>



                </section>
            </section>
        </section>
    </main>
    @include('partials.footer')
@endsection

@section('bodyscript')
    <script src="{{ asset('js/nav.js')}}"></script>
@endsection