@extends('layouts.init')

@section('title')
    Dulces Gustos - Receta
@endsection

@section('head')
    <link rel="stylesheet" href="{{ asset('css/nav.css')}}">
    <link rel="stylesheet" href="{{ asset('css/receta.css')}}">
@endsection



@section('content')

    @include('partials.header')
    <main class="container-fluid" id="prueba1">
        <section class="row bread-nav w-100">
            <nav aria-label="breadcrumb " class="col-12 w-100">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('index')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('recetas')}}">Recetas</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Receta</li>
                </ol>
            </nav>
        </section>
        <section class="row info-receta mt-2">
            <section class="col-lg-8 col-12 ">
                <section class="row content-receta">
                    <header class="col-12">
                        <h3 class="receta-title mt-2 pl-1 "> Fresa Diamante</h3>   
                    </header>
                     <main class="col-12  mb-1 info-des">
                        <img src="{{ asset('img/recetas/fresa-diamante.jpg')}}" class="img-thumbnail"  alt="">
                        <aside class="comple-info  ">
                            <section class="inf-receta d-flex">
                                <i class="icon-clock mr-1 hidden-lg-down"></i>
                                <p class="mr-2">Duración:</p>
                                <p> 30 min.</p>
                            </section>

                            <section class="inf-receta d-flex">
                                <i class="icon-star mr-1 hidden-lg-down"></i>
                                <p class="mr-2">Dificulta:</p>
                                <p> Facil.</p>
                            </section>
                            <section class="inf-receta d-flex">
                                <i class="icon-user-1 mr-1 hidden-lg-down"></i>
                                <p class="mr-2">Publicado:</p>
                                <p> Pepito Perez.</p>
                            </section>
                        </aside>
                     </main>
                     <footer class="col-12">
                         <p>Descripción: Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, eius itaque delectus ad magni illum sint voluptates, praesentium omnis, labore perspiciatis similique corrupti debitis iure enim fuga autem ipsam consequatur.</p>
                     </footer>
                </section>

                <section class="row content-receta">
                    <section class="card mb-3 col-12">
                        <section class="card-header"><h4 class="receta-title mt-2 pl-1 "> Ingredientes:</h4></section>
                        <section class="card-body">
                            <h5 class="card-title mt-2">Masa:</h5>
                            <section class="row">
                                <ul class="col-6 card-text list-ingr d-flex flex-column"  >
                                    <li class="d-flex">
                                        <i class="icon-check mr-2"></i>
                                        <p class="mr-3">Harina de Trigo </p>
                                        <p class="mr-3"> 100 </p>
                                        <p class="mr-1"> gr.</p>
                                    </li>
                                    <li class="d-flex">
                                        <i class="icon-check mr-2"></i>
                                        <p class="mr-3">Azucar </p>
                                        <p class="mr-3"> 200 </p>
                                        <p class="mr-1"> gr.</p>
                                    </li>
                                    <li class="d-flex">
                                        <i class="icon-check mr-2"></i>
                                        <p class="mr-3">Manteca </p>
                                        <p class="mr-3"> 200 </p>
                                        <p class="mr-1"> gr.</p>
                                    </li>  
                                </ul>
                                <ul class="col-6 card-text list-ingr d-flex flex-column"  >
                                    <li class="d-flex">
                                        <i class="icon-check mr-2"></i>
                                        <p class="mr-3">Agua </p>
                                        <p class="mr-3"> 100 </p>
                                        <p class="mr-1"> ml.</p>
                                    </li>
                                    <li class="d-flex">
                                        <i class="icon-check mr-2"></i>
                                        <p class="mr-3">Polvo de Hornear </p>
                                        <p class="mr-3"> 10 </p>
                                        <p class="mr-1"> gr.</p>
                                    </li>
                                    <li class="d-flex">
                                        <i class="icon-check mr-2"></i>
                                        <p class="mr-3">Sal </p>
                                        <p class="mr-3"> 5 </p>
                                        <p class="mr-1"> gr.</p>
                                    </li>  
                                </ul>
                            </section>
                            <h5 class="card-title mt-2">Relleno:</h5>
                            <section class="row">
                                <ul class="col-6 card-text list-ingr d-flex flex-column"  >
                                    <li class="d-flex">
                                        <i class="icon-check mr-2"></i>
                                        <p class="mr-3">Harina de Trigo </p>
                                        <p class="mr-3"> 100 </p>
                                        <p class="mr-1"> gr.</p>
                                    </li>
                                    <li class="d-flex">
                                        <i class="icon-check mr-2"></i>
                                        <p class="mr-3">Azucar </p>
                                        <p class="mr-3"> 200 </p>
                                        <p class="mr-1"> gr.</p>
                                    </li>
                                    <li class="d-flex">
                                        <i class="icon-check mr-2"></i>
                                        <p class="mr-3">Manteca </p>
                                        <p class="mr-3"> 200 </p>
                                        <p class="mr-1"> gr.</p>
                                    </li>  
                                </ul>
                                <ul class="col-6 card-text list-ingr d-flex flex-column"  >
                                    <li class="d-flex">
                                        <i class="icon-check mr-2"></i>
                                        <p class="mr-3">Agua </p>
                                        <p class="mr-3"> 100 </p>
                                        <p class="mr-1"> ml.</p>
                                    </li>
                                    <li class="d-flex">
                                        <i class="icon-check mr-2"></i>
                                        <p class="mr-3">Polvo de Hornear </p>
                                        <p class="mr-3"> 10 </p>
                                        <p class="mr-1"> gr.</p>
                                    </li>
                                    <li class="d-flex">
                                        <i class="icon-check mr-2"></i>
                                        <p class="mr-3">Sal </p>
                                        <p class="mr-3"> 5 </p>
                                        <p class="mr-1"> gr.</p>
                                    </li>  
                                </ul>
                            </section>
                        </section>
                    </section>
                </section>

                <section class="row">
                    <section class="card col-12  mb-3" >
                        <section class="card-header"><h4 class="receta-title mt-2 pl-1 ">Preparación:</h4></section>
                        <section class="card-body">
                            <h5 class="card-title mt-2">Paso 1:</h5>
                            <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta eligendi dolorem qui corporis corrupti deleniti enim, facere quia minus ipsam explicabo atque sint quibusdam, necessitatibus quod sequi ullam assumenda vitae?</p>
                            <h5 class="card-title mt-2">Paso 2:</h5>
                            <p class="card-text">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quidem, nisi?</p>
                            <h5 class="card-title mt-2">Paso 3:</h5>
                            <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta eligendi dolorem qui corporis corrupti deleniti enim, facere quia minus ipsam explicabo atque sint quibusdam, necessitatibus quod sequi ullam assumenda vitae?</p>
                            <h5 class="card-title mt-2">Paso 4:</h5>
                            <p class="card-text">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quidem, nisi?</p>
                            <h5 class="card-title mt-2">Paso 5:</h5>
                            <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta eligendi dolorem qui corporis corrupti deleniti enim, facere quia minus ipsam explicabo atque sint quibusdam, necessitatibus quod sequi ullam assumenda vitae?</p>
                            <h5 class="card-title mt-2">Paso 6:</h5>
                            <p class="card-text">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quidem, nisi?</p>
                        </section>
                    </section>

                </section>
            </section>
            <section class="col-12 d-flex col-lg-4">
                <section class="row content-receta">
                    <section class="card mx-lg-3  mb-3 col-12 ">
                        <section class="card-header text-center"><h4 class="receta-title mt-2 pl-1 "> Categorias</h4></section>
                        <section class="card-body">
                            <h5 class="card-title mt-2">Esta receta se encuentra:</h5>
                            <section class="row">
                                <ul class="col-6 card-text list-ingr d-flex flex-column"  >
                                    <li class="d-flex">
                                        <i class="icon-check mr-2"></i>
                                        <p class="mr-3">Celiaco</p>
                                    </li>
                                    <li class="d-flex">
                                        <i class="icon-check mr-2"></i>
                                        <p class="mr-3">Galletas</p>
                                    </li>
                                    <li class="d-flex">
                                        <i class="icon-check mr-2"></i>
                                        <p class="mr-3">Media</p>
                                    </li>  
                                </ul>
                                <ul class="col-6 card-text list-ingr d-flex flex-column"  >
                                    <li class="d-flex">
                                        <i class="icon-check mr-2"></i>
                                        <p class="mr-3">Boda</p> 
                                    </li>
                                    <li class="d-flex">
                                        <i class="icon-check mr-2"></i>
                                        <p class="mr-3">Cumpleaños </p> 
                                    </li>
                                    <li class="d-flex">
                                        <i class="icon-check mr-2"></i>
                                        <p class="mr-3">Fitness</p>
                                    </li>  
                                </ul>
                            </section>
                        </section>
                    </section>
                    <section class="card mr-4   mb-3 mx-lg-3 col-lg-12 col-5">
                        <section class="card-header text-center"><h4 class="receta-title mt-2 pl-1 "> Convertidor</h4></section>
                        <section class="card-body">
                            <h5 class="card-title mt-2">Convierta a sus medidas:</h5>
                            <form class="row" action="">
                                <section class="col-12 form-group">

                                    <input type="text" name="cantidad" id="cantidad" class="form-control" placeholder="Ingrese la cantidad a convertir.">
                                </section>
                                <section class="col-6 form-group">
                                    <label class="form-check-control" for="convertir-de">De: </label>
                                    <select  name="convertir-de" id="convertir-de" class="ml-2  custom-select" ><option value="1" selected>unid</option><option value="2">gr</option><option value="3">ml</option><option value="4">oz</option><option value="5">cdita</option><option value="6">cda</option><option value="7">taza</option></select>
                                </section>
                                <section class="col-6 form-group">
                                    <label class="form-check-control" for="convertir-a">A: </label>
                                    <select  name="convertir-a" id="convertir-a" class="ml-2  custom-select" ><option value="1" selected>unid</option><option value="2">gr</option><option value="3">ml</option><option value="4">oz</option><option value="5">cdita</option><option value="6">cda</option><option value="7">taza</option></select>
                                </section>
                                <section class="col-12 d-flex justify-content-center">
                                    <button type="button" class="btn btn-success">Convertir</button>
                                </section>
                                <section class="col-12  resultado">
                                    <label for="resultado"class="form-check-label">Resultado: </label>
                                    <input type="text" class="form-control" name="resultado" id="resultado" disabled>
                                </section>
                            </form>
                        </section>
                    </section>
                    <section class="card ml-4  mb-3 mx-lg-3 col-lg-12 col-5">
                        <section class="card-header text-center"><h4 class="receta-title mt-2 pl-1 "> Sustituye</h4></section>
                        <section class="card-body">
                            <h5 class="card-title mt-2">Sustituye el ingrediente que te falte:</h5>
                            <form class="row" action="">
                                <section class="col-12 form-group">
                                    <label for="ingrediente" class="form-check-control">Ingrediente: </label>
                                    <select  name="ingrediente" id="ingrediente" class="ml-2  custom-select" ><option value="1" selected>Harina de Trigo</option><option value="2">Azucar</option><option value="3">Manteca</option><option value="4">Agua</option><option value="5">Polvo de Hornear</option><option value="6">Sal</option></select>
                                </section>
                                <section class="col-12 form-group">
                                    <label class="form-check-control" for="sustituto">Sustituyelo por:</label>
                                    <select  name="sustituto" id="sustituto" class="ml-2  custom-select" aria-readonly="true"><option value="1" selected>Harina de Trigo</option><option value="2">Azucar</option><option value="3">Manteca</option><option value="4">Agua</option><option value="5">Polvo de Hornear</option><option value="6">Sal</option></select>
                                </section>
                            </form>
                        </section>
                    </section>


                    <section class="col-12 mx-lg-3 embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/watch?v=dzm1BTEl9_A&t=12s" frameborder="0" allowfullscreen></iframe>

                    </section>
                </section>
            </section>
        </section>
    </main>
    @include('partials.footer')
@endsection

@section('bodyscript')
    <script src="{{ asset('js/nav.js')}}"></script>
@endsection