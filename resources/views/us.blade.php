@extends('layouts.init')

@section('title')
    Dulces Gustos - Nosotros
@endsection

@section('head')
    <link rel="stylesheet" href="{{ asset('css/nav.css')}}">
    <link rel="stylesheet" href="{{ asset('css/nostros.css')}}">
@endsection



@section('content')

    @include('partials.header')
    <main class="container-fluid">
        <section class="row bread-nav w-100">
            <nav aria-label="breadcrumb " class="col-12 w-100">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('index')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Nosotros</li>
                </ol>
            </nav>
        </section>
        <section id="prueba1" class="row no-gutters  sobre-nosotros">
            <section class="col-12 flex-column sobre-nosotros-col d-flex justify-content-center align-items-center ">
                <img src="img/nosotros-img.jpg" alt="">
                <span>Nosotros</span>
            </section>

        </section>

        <section class="row  content-1 mt-3">
            <section class="col-6 d-flex img-nosotros1 justify-content-center align-items-center">
                <h1 class="title">Dulces Gustos</h1>
            </section>
            <section class="col-6  d-block">
                <section class="row">
                    <section class="col-1 d-flex align-items-center"><i class="icon-ok-outline"></i></section>
                    <section class="col-10">
                        <p class="img-info">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ab rerum
                            molestias, fugit laborum, temporibus provident ducimus tempore eligendi illo, mollitia eos
                            sed beatae suscipit. Aliquam voluptatibus quasi quam maxime obcaecati?</p>
                    </section>
                </section>
                <section class="row">
                    <section class="col-1 d-flex align-items-center"><i class="icon-ok-outline"></i></section>
                    <section class="col-10">
                        <p class="img-info">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ab rerum
                            molestias, fugit laborum, temporibus provident ducimus tempore eligendi illo, mollitia eos
                            sed beatae suscipit. Aliquam voluptatibus quasi quam maxime obcaecati?</p>
                    </section>
                </section>
                <section class="row">
                    <section class="col-1 d-flex align-items-center"><i class="icon-ok-outline"></i></section>
                    <section class="col-10">
                        <p class="img-info">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ab rerum
                            molestias, fugit laborum, temporibus provident ducimus tempore eligendi illo, mollitia eos
                            sed beatae suscipit. Aliquam voluptatibus quasi quam maxime obcaecati?</p>
                    </section>
                </section>
                <section class="row">
                    <section class="col-1 d-flex align-items-center"><i class="icon-ok-outline"></i></section>
                    <section class="col-10">
                        <p class="img-info">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ab rerum
                            molestias, fugit laborum, temporibus provident ducimus tempore eligendi illo, mollitia eos
                            sed beatae suscipit. Aliquam voluptatibus quasi quam maxime obcaecati?</p>
                    </section>
                </section>
            </section>
        </section>

        <section class=" row content-2 mt-3">
            <section class="col-12 text-center">
                <h4>Especialitas en productos de alta calidad en las areas.</h4>
            </section>
            <section class="col-12 ">
                <section class="row content-2-img d-flex justify-content-around">
                    <section class="col-4 img-1 text-center">
                        <img src="img/pan.png" alt="pan">
                        <p class="img-title">Panaderia</p>
                        <p class="img-info"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis,
                            consequuntur!</p>
                    </section>
                    <section class="col-4 img-2 text-center">
                        <img src="img/repostery.png" alt="pan">
                        <p class="img-title">Pasteleria</p>
                        <p class="img-info"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis,
                            consequuntur!</p>

                    </section>
                    <section class="col-4 img-3 text-center">
                        <img src="img/cake.png" alt="pan">
                        <p class="img-title">Reposteria</p>
                        <p class="img-info"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis,
                            consequuntur!</p>

                    </section>

                </section>
            </section>
        </section>
    </main>

    @include('partials.footer')
@endsection

@section('bodyscript')
    <script src="{{ asset('js/nav.js')}}"></script>
@endsection