<section class="col-4 item item-visible  col-lg-3 img-thumbnail items-1">
    <a href="{{ route('receta')}}">
        <p class="title-receta">Fresa Diamante</p>
        <section class="caption">
            <section class="blur"></section>
            <section class="d-flex flex-column align-items-center justify-content-around caption-text">
                <h1>Fresa Diamante</h1>
                <p class="px-2">Ricas galletas, con masa de hojaldre y una dulce fresa en medio.</p>
            </section>
        </section>
    </a>
</section>