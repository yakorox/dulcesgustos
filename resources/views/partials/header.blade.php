<header class="container-fluid d-flex flex-column ">
        <!-- Menu Principal  para el tamaño LG-->
        <section class="row nav align-items-stretch mx-0 fixed-top " id="navs">
            <section class="  contenedor-enlace align-items-stretch hidden-sm-down col-md-auto d-flex align-items-center justify-content-center justify-content-lg-start logo">
                <a class="" href="{{ route('index')}}">
                    <img src="{{asset('img/logo.png')}}" alt="Dulces Gustos">
                </a>
            </section>
            <!-- ojooo-->
            <section class="col-12 mini-menu buscar-1 hidden-md-up">
                <form action="">
                    <section class="row no-gutters align-items-center">
                        <section class="columna col-10">
                            <input type="text" name="" id="" placeholder="Recetas,Ingrediente,Tipo de Dieta">
                        </section>
                        <section class="columna col-2 align-items-center justify-content-center text-center">
                            <button><i class="icon-search-outline"></i></button>
                        </section>
                    </section>
                </form>
            </section>

            <section class="col-6 hidden-md-up mini-menu reg-usu align-items-stretch">
                <a href="{{ route('registro')}}" class="d-flex  text-center  flex-column ">
                    <i class="icono icon-pencil"></i>
                    <span>¿Eres Nuevo?, Regístrate!!</span>
                </a>
            </section>
            <!--Fin ojo-->

            <nav class="  menu contenedor-enlace flex-wrap flex-md-nowrap mini-menu flex-wrap  d-flex col-12 col-md-5 ">
                <a href="{{ route('recetas')}}" class="c-1 col-12 col-md-4 justify-content-center enlace d-flex align-items-center">
                    <section class="d-flex flex-column text-center">
                        <span>Recetas</span>
                        <i class="icono icon-book"></i>
                    </section>
                </a>
                <a href="{{ route('tips')}}" class="c-2 col-12 col-md-4 justify-content-center enlace d-flex align-items-center">
                    <section class="d-flex flex-column text-center">
                        <span>Tips</span>
                        <i class="icono icon-magic"></i>
                    </section>
                </a>
                <a href="{{ route('nosotros')}}" class="c-3 col-12 col-md-4 justify-content-center enlace d-flex align-items-center">
                    <section class="d-flex flex-column text-center">
                        <span>Nosotros</span>
                        <i class="icono icon-child"></i>
                    </section>
                </a>
            </nav>
            <!-- AQUI VA LO QUE SE VA A MODIFICAR-->
            <section class="col-6 icons mini-menu log  text-center col-md-1 d-flex align-items-stretch justify-content-center align-items-center colum-icons">
                <a href="#" data-toggle="modal" data-target="#login" class="d-flex  enlace align-items-center align-items-stretch flex-column ">
                    <i class="icono icon-user-1"></i>
                    <span>Iniciar Sesión</span>
                </a>
            </section>
            <section class="col-12 icons mini-menu help  text-center col-md-1 d-flex align-items-stretch justify-content-center align-items-center colum-icons">
                <a href="#" data-toggle="modal" data-target="#help" class="d-flex  align-items-center align-items-stretch flex-column ">
                    <i class="icono icon-help"></i>
                    <span>Ayuda</span>
                </a>
            </section>
        </section>

        <a href="#" class="fondo-enlace hidden-md-up" id="fondo-enlace"></a>

        <section class="row hidden-sm-down  extra-btn d-flex rounded-top justify-content-between align-items-stretch">
            <section class="col-md-7 col-12 buscar  flex-wrap flex-sm-nowrap ">
                <form action="">
                    <section class="row no-gutters align-items-center">
                        <section class="columna col-10">
                            <input type="text" name=""  placeholder="Recetas,Ingrediente,Tipo de Dieta">
                        </section>
                        <section class="columna col-2">
                            <button><i class="icon-search-outline"></i></button>
                        </section>
                    </section>
                </form>
            </section>
            <section class="col-md-4 col-lg-3 col-12  reg-col d-flex align-items-stretch align-items-center">
                <a class="registro d-flex flex-column text-center align-items-center" href="{{ route('registro')}}">
                    <i class="icono icon-pencil"></i>
                    <span>¿Eres Nuevo?, Regístrate!</span>

                </a>
            </section>
        </section>


        <!-- Final del Menu Principal para el tamaño LG-->
        <section class="row btn-menu d-flex justify-content-between hidden-md-up ">
            <a href="{{ route('index')}}" class="logo-menu col-2 d-flex flex-column justify-content-start">
                <span>DG</span>
            </a>
            <a href="#" class="menu-icon col-10 d-flex flex-column justify-content-center align-items-end" id="btn-menus">
                <i class="icon-menu-outline icon-lg"></i>
            </a>
        </section>

        <aside id="Social-nav" class="social-nav sticky-container">
            <ul>
                <li class="shop">
                    <a href="#">
                        <i class="icon-basket"></i>
                        <span class="shop-text">Ver Tienda</span>
                    </a>
                </li>
                <li class="facebook">
                    <a href="https://www.facebook.com" target="_blank">
                        <i class="icon-facebook-1"></i>
                        <span class="name-social">Facebook</span>
                    </a>
                </li>
                <li class="instagram">
                    <a href="https://www.instagram.com" target="_blank">
                        <i class="icon-instagram"></i>
                        <span class="name-social">Instagram</span>
                    </a>
                </li>
                <li class="youtube">
                    <a href="https://www.youtube.com/" target="_blank">
                        <i class="icon-youtube"></i>
                        <span class="name-social">Youtube</span>
                    </a>
                </li>
                <li class="twitter">
                    <a href="https://twitter.com/GustosDulces" target="_blank">
                        <i class="icon-twitter-bird"></i>
                        <span class="name-social">Twitter</span>
                    </a>
                </li>
            </ul>
        </aside>

        <section class="row modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <section class="col-12 modal-dialog" roles="document">
                <section class="modal-content">
                    <section class="modal-header d-flex justify-content-center">
                        <h4 class="modal-title" id="myModalLabel">Inicio de Sesión</h4>
                    </section>
                    <section class="modal-body login">
                        <form action="">
                            <section class="form-group">
                                <label for="email" class="form-check-label pl-1">Correo</label>
                                <input class="form-control" type="email" name="email" id="email" placeholder="example@dulcesgustos.mail"
                                    required>
                            </section>
                            <section class="form-group">
                                <label for="pass" class="form-check-label pl-1">Clave</label>
                                <input class="form-control" type="password" name="pass" id="pass" placeholder="Contraseña"
                                    required>
                            </section>
                            <section class="d-flex justify-content-between">
                                <a href="registro.html">¿No estas Registrado?</a>
                                <a href="#">¿Olvidaste tu Clave?</a>
                            </section>
                            <section class="d-flex justify-content-center">
                                <button class="btn btn-success mt-3"> Iniciar Sesión</button>
                            </section>
                        </form>
                    </section>
                </section>
            </section>
        </section>

        <!--Ojo cambio-->
        <section class="row modal fade" id="help" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <section class="col-12 modal-dialog" roles="document">
                <section class="modal-content help-modal">
                    <section class="modal-header d-flex justify-content-center">
                        <h4 class="modal-title" id="myModalLabel">Ayuda</h4>
                    </section>
                    <section class="modal-body">
                        <h3>Si necesitas ayuda, Contáctanos</h3>
                        <form action="">
                            <section class="form-group">
                                <label for="email-contacto" class="form-check-label pl-1">Correo</label>
                                <input class="form-control" type="email-contacto" name="email-contacto" id="email-contacto"
                                    placeholder="Tu Correo" required>
                            </section>
                            <section class="form-group">
                                <label for="asunto" class="form-check-label pl-1">Asunto</label>
                                <input class="form-control" type="asunto" name="asunto" id="asunto" placeholder="Asunto."
                                    required>
                            </section>
                            <section class="form-group">
                                <label for="descr" class="form-check-label pl-1">Descripción: </label>
                                <textarea name="descr" id="descr" maxlength="50" class="form-control noresize"></textarea>
                            </section>

                            <button type="button" class="btn btn-success">Enviar</button>

                        </form>
                    </section>
                </section>
            </section>
        </section>
        <!--FIn cambio-->
    </header>