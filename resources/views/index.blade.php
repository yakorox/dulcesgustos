@extends('layouts.init')

    @section('title')
        Dulces Gustos
    @endsection

    @section('head')
        <link rel="stylesheet" href="{{ asset('css/nav.css')}}">
        <link rel="stylesheet" href="{{ asset('css/style.css')}}">
    @endsection


    @section('content')
        @include('partials.header')
            <main class="container-fluid">
            <section class="row bread-nav w-100">
                <nav aria-label=" col-12 breadcrumb w-100">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page">Home</li>
                    </ol>
                </nav>
            </section>

            <section id="prueba1" class="row  justify-content-center align-items-center">
                <section class="col-12 col-lg-6 welcome d-flex column-flex text-center align-items-center justify-content-center"
                    id="welcome">
                    <section class="row d-flex justify-content-center welcome-row">
                        <section class="col-12 justify-content-center align-items-end d-flex column-flex fil-inter">
                            <span class="welcome-text d-flex align-items-center"><i class="icon-bandcamp icon"></i>Bienvenidos
                                a<i class="icon icon-bandcamp "></i></span>
                        </section>

                        <section class="col-12 d-flex justify-content-center nombre-empresa-col">
                            <h1 class="nombre-empresa">Dulces Gustos</h1>
                        </section>
                        <section class="col base-nombre">
                            <i class="icon-snowflake-o ics"></i>
                        </section>
                    </section>
                </section>

                <section class="col-12 col-lg-6 wwm col-quest d-flex column-flex text-center align-items-center justify-content-center"
                    id="wwm">
                    <section class="row quest-1 justify-content-center d-flex align-items-center">
                        <section class="col-12  justify-content-center align-items-end d-flex">
                            <span class="question-1">¿Qué quieres preparar hoy?</span>
                        </section>
                        <section class="col-12 ">
                            <section class="row no-gutters justify-content-center align-items-start">
                                <section class="col-12 bus-2 ">
                                    <form action="">
                                        <input type="text" name=""  placeholder="Galletas/Tortas/Chocolate/etc...">
                                    </form>
                                </section>
                                <section class="col-6">
                                    <button class="btn btn-success" id="comenzar">! Comenzar ¡</button>
                                </section>
                            </section>
                        </section>
                    </section>
                </section>

                <section class="col-12 hidden-xs-up wwm col-quest d-flex column-flex text-center align-items-center justify-content-center"
                    id="wwm-1">
                    <section class="row quest-1 justify-content-center d-flex align-items-center">
                        <section class="col-12  justify-content-center align-items-end d-flex">
                            <span class="question-1">¿Posee alguna alergia?</span>
                        </section>
                        <section class="col-12 ">
                            <section class="row no-gutters justify-content-between align-items-start">
                                <section class="col-12 bus-2 ">
                                    <form action="">
                                        <input type="text" name=""  placeholder="Si/No">
                                    </form>
                                </section>

                                <section class="col-3 next-step">
                                    <button type="button" class="btn btn-danger  " id="step1-prev">
                                        <i class="icon-left-open-outline"></i>
                                    </button>
                                </section>
                                <section class="col-3 next-step">
                                    <button class="btn btn-success " id="step1-next">
                                        <i class="icon-right-open-outline"></i>
                                    </button>
                                </section>
                            </section>
                        </section>
                    </section>
                </section>

                <section class="col-12 hidden-xs-up wwm col-quest d-flex column-flex text-center align-items-center justify-content-center"
                    id="wwm-2">
                    <section class="row quest-1 justify-content-center d-flex align-items-center">
                        <section class="col-12  justify-content-center align-items-end d-flex">
                            <span class="question-1">¿Algun ingrediente adicional?</span>
                        </section>
                        <section class="col-12 ">
                            <section class="row no-gutters justify-content-between align-items-start">
                                <section class="col-12 bus-2 ">
                                    <form action="">
                                        <input type="text" name=""  placeholder="Mani, Almendra, etc">
                                    </form>
                                </section>

                                <section class="col-3 next-step">
                                    <button type="button" class="btn btn-danger  " id="step2-prev">
                                        <i class="icon-left-open-outline"></i>
                                    </button>
                                </section>
                                <section class="col-3 next-step">
                                    <button class="btn btn-success " id="step2-next">
                                        <i class="icon-right-open-outline"></i>
                                    </button>
                                </section>
                            </section>
                        </section>
                    </section>
                </section>

                <section class="col-12 hidden-xs-up wwm col-quest d-flex column-flex text-center align-items-center justify-content-center"
                    id="wwm-3">
                    <section class="row quest-1 justify-content-center d-flex align-items-center">
                        <section class="col-12  justify-content-center align-items-end d-flex">
                            <span class="question-1">¿Algun ingrediente que no desee utilizar?</span>
                        </section>
                        <section class="col-12 ">
                            <section class="row no-gutters justify-content-between align-items-start">
                                <section class="col-12 bus-2 ">
                                    <form action="">
                                        <input type="text" name=""  placeholder="Leche, Huevo, Azucar, etc">
                                    </form>
                                </section>

                                <section class="col-3 next-step">
                                    <button type="button" class="btn btn-danger  " id="step3-prev">
                                        <i class="icon-left-open-outline"></i>
                                    </button>
                                </section>
                                <section class="col-3 next-step">
                                    <button class="btn btn-success " id="step3-next">
                                        <i class="icon-right-open-outline"></i>
                                    </button>
                                </section>
                            </section>
                        </section>
                    </section>
                </section>

                <section class="col-12 hidden-xs-up wwm col-quest d-flex column-flex text-center align-items-center justify-content-center"
                    id="wwm-4">
                    <section class="row quest-1 justify-content-center d-flex align-items-center">
                        <section class="col-12  justify-content-center align-items-end d-flex">
                            <span class="question-1">¿Algun producto para gusto especifico?</span>
                        </section>
                        <section class="col-12 ">
                            <section class="row no-gutters justify-content-between align-items-start">
                                <section class="col-12 bus-2 ">
                                    <form action="">
                                        <input type="text" name=""  placeholder="Diabetico, Fitness, Celiaco, etc">
                                    </form>
                                </section>

                                <section class="col-3 next-step">
                                    <button type="button" class="btn btn-danger  " id="step4-prev">
                                        <i class="icon-left-open-outline"></i>
                                    </button>
                                </section>
                                <section class="col-3 next-step">
                                    <a href="{{ route('recetas')}}">
                                        <button class="btn btn-success " id="step4-next">
                                            <i class="icon-right-open-outline"></i>
                                        </button>
                                    </a>
                                </section>
                            </section>
                        </section>
                    </section>
                </section>
            </section>

            <section class="row  ">
                <section class="col-12 col-lg-9 steps">
                    <section class="row no-gutters d-flex carousel carousel-fade slide " id="principal-carousel" data-ride="carousel">
                        <ol class="col-2 carousel-list-items carousel-indicators justify-content-center d-flex  flex-wrap flex-column">
                            <li data-target="#principal-carousel" data-slid-to="0" data-interval="1" class="active d-flex justify-content-center align-items-center steps-items ">
                                <label>Compartir</label>
                            </li>
                            <li data-target="#principal-carousel" data-slid-to="1" data-interval="1" class="d-flex justify-content-center align-items-center steps-items ">
                                <label>PASO 1</label>
                            </li>
                            <li data-target="#principal-carousel" data-slid-to="2" data-interval="1" class="d-flex justify-content-center align-items-center steps-items ">
                                <label>PASO 2</label>
                            </li>
                            <li data-target="#principal-carousel" data-slid-to="3" data-interval="1" class="d-flex justify-content-center align-items-center steps-items ">
                                <label>PASO 3</label>
                            </li>
                            <li data-target="#principal-carousel" data-slid-to="4" data-interval="1" class="d-flex justify-content-center align-items-center steps-items ">
                                <label>PASO 4</label>
                            </li>
                        </ol>
                        <section class="col-10 carousel-inner carousel-canvas">
                            <section class="carousel-item  carousel-content content-0 active">
                                <article class="row no-gutters  d-flex justify-content-center">
                                    <h4 class="col-12 text-center mt-3">Comparte tus Secretos</h4>
                                    <p> Esa receta que te enorgullece, que a pasado en tu familia
                                        de generación en generación, o esos secretos que hacen tu
                                        preparación especial compartela con nostros.</p>
                                    <h4 class="col-12 text-center">ES MUY FACIL</h4>
                                </article>
                            </section>

                            <section class="carousel-item  carousel-content content-1 ">
                                <section class="row  ">
                                    <h5 class="col-12">Inicia sesión</h5>
                                </section>

                            </section>
                            <section class="carousel-item  carousel-content content-2 ">
                                <section class="row  ">
                                    <h5 class="col-12">Compartir Recetas...</h5>
                                </section>
                            </section>
                            <section class="carousel-item  carousel-content content-3 ">
                                <section class="row  ">
                                    <h5 class="col-12">Completa los campos</h5>
                                </section>
                            </section>
                            <section class="carousel-item  carousel-content content-4 ">
                                <section class="row  ">
                                    <h5 class="col-12">Compartir </h5>
                                </section>
                            </section>
                        </section>
                    </section>
                </section>
                <section class="hidden-md-down col-lg-3 content-twitter">
                    <a class="twitter-timeline" href="https://twitter.com/GustosDulces?ref_src=twsrc%5Etfw">Tweets by
                        GustosDulces</a>
                    <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                </section>
            </section>

            <section class="row d-flex notices">
                <section class=" col-6  receta-destacada hero-image">
                    <a href="#" class="d-flex justify-content-between flex-column align-items-between">
                        <header class="hero-title">
                            <h1> Receta destacada de la semana</h1>
                        </header>

                        <footer class="hero-text">
                            <h3 class="title">Ponquesitos</h3>
                            <p class="info-receta">Dulces gustos comparte la receta de ponquesitos con una gran variedad de sabores y colores.!</p>
                        </footer>
                    </a>
                </section>

                <section class="col-6 col-md-3 hero-image receta-nueva">
                    <a href="#" class="d-flex justify-content-between wrap flex-column align-items-between">
                        <header class="hero-title">
                            <h1> Nueva receta</h1>
                        </header>

                        <footer class="hero-text">
                            <h3 class="title">Souffle Glace</h3>
                            <p class="info-receta">Souffle Glace es un postre muy delicioso, con un solo bocado endulzas la vida.!</p>
                        </footer>
                    </a>
                </section>

                <section class="col-12 col-md-3 col-mini-sec d-flex flex-column">
                    <section class="row mini-sec d-flex">
                        <section class="col-12 hero-image tips-nuevo">
                            <a href="{{ route('tips')}}" class="d-flex justify-content-between wrap flex-column align-items-between">
                                <header class="hero-title">
                                    <h1> Tip Nuevo</h1>
                                </header>

                                <footer class="hero-text">
                                    <h3 class="title">Preservar la levadura</h3>
                                    <p class="info-receta">El cuidado de la levadura ayuda en el momento de preparar cualuier pan. </p>
                                </footer>
                            </a>
                        </section>
                        <section class="col-12 hero-image receta-estrella">
                            <a href="{{ route('receta')}}" class="d-flex justify-content-between wrap flex-column align-items-between">
                                <header class="hero-title">
                                    <h1> Receta Estrella</h1>
                                </header>

                                <footer class="hero-text">
                                    <h3 class="title">Fresa Diamante</h3>
                                    <p class="info-receta">Una delicia a base de hojaldre. </p>
                                </footer>
                            </a>
                        </section>
                    </section>
                </section>
            </section>
        </main>

    @include('partials.footer')
    @endsection

    @section('bodyscript')
        <script src="{{ asset('js/nav.js')}}"></script>
        <script src="{{ asset('js/principal.js')}}"></script>
    @endsection